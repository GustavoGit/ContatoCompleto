//
//  DetalhesViewController.swift
//  Contato
//
//  Created by Eder Andrade on 31/08/22.
//

import UIKit

protocol DetalhesViewControllerDelegate {
    func excluirContato(index: Int)
}

class DetalhesViewController: UIViewController {

    @IBOutlet weak var nomeLabel: UILabel!
    @IBOutlet weak var numeroLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var enderecoLabel: UILabel!
    
    public var index: Int?
    public var contato: Contato?
    public var delegate: DetalhesViewControllerDelegate?
    public var contatoDelegate: ContatoViewControllerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        title = contato?.nome
        
        nomeLabel.text = contato?.nome
        numeroLabel.text = contato?.telefone
        emailLabel.text = contato?.email
        enderecoLabel.text = contato?.endereco
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "editarContato" {
            let contatoViewController = segue.destination as? ContatoViewController
            contatoViewController?.contato = contato
            contatoViewController?.delegate = self
        }
    }
    
    @IBAction func excluirContatoTap(_ sender: Any) {
        
        let alert = UIAlertController(title: "Title", message: "Message", preferredStyle: .alert)
   
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

        alert.addAction(UIAlertAction(title: "Deletar", style: .default, handler: { (action) in
            self.delegate?.excluirContato(index: self.index!)
            self.navigationController?.popViewController(animated: true)
        }))
                        
        self.present(alert, animated: true, completion: nil)
    }
}

extension DetalhesViewController: ContatoViewControllerDelegate {
    func salvarNovoContato(contato: Contato) {
        
    }
    
    func editarContato() {
        title = contato?.nome
        
        nomeLabel.text = contato?.nome
        numeroLabel.text = contato?.telefone
        emailLabel.text = contato?.email
        enderecoLabel.text = contato?.endereco
        
        contatoDelegate?.editarContato()
    }
}
